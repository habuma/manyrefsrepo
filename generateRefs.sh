#!/bin/bash

for i in {1..1100}
do
  UUID="$(uuidgen)"
  echo $UUID
  git tag -a $UUID -m "My version $UUID"
done

# for i in {1..16}
# do
#   UUID="$(uuidgen)"
#   echo $UUID
#   git checkout -b $UUID
# done
